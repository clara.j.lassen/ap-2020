## Algorithmic Procedures | Individual work + Group work

(with Andreas, Anne E. & Mikkel Dahlin)

**Challenges between simplicity at the level of communication and complexity at the level of algorithmic procedure**

Working with methods such as flowcharts in a programming process has its advantages and challenges. It gives clear instructions as to what the program has to accomplish and in what order, which is helpful when programmers has to write the code for a large-scaled project. Especially when the project is created by a team, it’s important that communication between the people involved is very clear. In this case the flowchart visualises the idea and - if it’s drawn in a neither too complex nor to simple scale - communicates the plan or instructions to not only the programmers but the buyer as well as the user.

But these algorithmic methods has received a lot of critic over the years, as either not being complex enough or being overly complex without being helpful in any way. In the text  “The Multiple Meanings of a Flowchart” by Nathan Ensmenger (2016) he mentions that Ned Chapin suggested that a flowchart with too much detail wasn’t more useful than the code of program itself. In 1963. Donald Knuth mocked the oversimplified flowcharts and argued that it would just be easier for the reader to read the actual program (Ensmenger, 2016). And as it’s also stated in Ensmengers text, many programmers today feel the same way by either not drawing a flowchart at all, or drawing it by the end of the project for the formality of it all. Frederick Brooks denied having ever known “an experienced programmer who routinely made detailed flowcharts before beginning to write programs” (Ensmenger, 2016:323).

However, Beth Bechky argued in her analysis of engineering drawings, that it’s more so about having an abstract picture rather than having a concrete formula of a building. Bechky is talking about engineering blueprints, but the statement is being referred to as the same matter with algorithmic procedures. 


>  “... the drawings “needed to remain abstract not only for their use as an epistemic tool, but also for reasons of boundary maintenance and task control.“... lack of definitive clarity on the part of these drawings was a feature, not a flaw...” (Ensmenger, 2016:337).

As Taina Bucher argues in her text “If… THEN: Algorithmic Power and Politics” (2018), algorithmic procedures are a way of simplifying and communicating a problem both as computational systems and indicators of how society is built. 


>  “Designing an algorithm to perform a certain task implies a simplification of the problem at hand” (Bucher, 2018:22).

The complexity of algorithms have the ability to say a lot about politics and our society today as well as how it has changed throughout the years, by looking at old algorithmic procedures. This also implies that algorithms contain a massive amount of power and control many aspects of the internet, with the capability to ultimately change how people think and perceive the world. 

To quote Bucher’s text: “*My hope is to show how we might better understand the power and politics of algorithms if we try to take a more holistic approach by acknowledging the complexity and multiplicity of algorithms, machine learning and big data.*” (Bucher, 2018:27).


**Technical challenges for the two ideas, as well as the value of the individual and the group's flowchart**

![screenshot](groupidea1.png)

Group flowchart 1

![screenshot](groupidea2.png)

Group flowchart 2

![screenshot](individualidea.png)

Individual flowchart - miniex6

The two ideas we as a group picked out are (1) a game involving smart homes/AI by working with topics as machine learning/algorithms and data capture, and (2) an interactive story about the American election, where the user’s choices have consequences for how the future is going to look like, also covering the topics data-capture and algorithms. 

Idea number 1 comes with a great set of technical challenges, since we want to create two different rooms with a big set of objects the user can interact with, as well as a specific story we have to incorporate in our program. We want to make it as easy to understand as possible to get our point across completely. All objects such as a bed, lamp, computer, tv, fridge etc. are all being interacted with in different ways. Even though the story is the most important part of the program, it’s not linear - meaning, the user has the freedom to explore the game as they want. But a way to make sure the story goes on as we intent, is to give the user clear instructions or objectives as to how progress in the game is being made. Our group started a bit early with the brainstorm and have already chosen this as our idea for the final project. We made the anatomy of the program, which gives us an idea as to how we’re going to handle the technical aspects of the challenges. As you can see on flowchart 1 we have visualised the outline of our program and its objects being divided into the two rooms, and what progress the interaction of the objects makes. 

Idea number 2 is a bit more simple, technically speaking, because it’s even more focused on the story rather than the visuals. The program would raise a list of questions, and depending on which choice they make, the consequences would appear in the media, and the program would move on to the next questions, and so on. The technical challenges would be to create multiple different endings, depending on the choices taken, but that itself would easily be illustrated in a flowchart - as many quizzes/ tests on the internet are being visualised today. We drew a simplified version of this in flowchart number 2, which involves two different endings depending on which of the two endings the choices are leaning towards. 

The big difference between the individual and the group’s flowcharts is having to draw an algorithm after the program has been made versus while it’s still in the idea generation process. When drawing a flowchart for a program I made weeks ago, I didn’t feel like it gave me anything, rather than a summary of my program. But creating a flowchart for just an idea was a big part of the creative process, and stated some boundaries for the group’s project as to which algorithm the program should follow. 

