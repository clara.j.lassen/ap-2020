
![screenshot](screenshotMiniex5.png)


[Link to my miniEx 5](https://clara.j.lassen.gitlab.io/ap-2020/Mini%20Exercises/miniEx5/)

[Link to repository](https://gitlab.com/clara.j.lassen/ap-2020/-/blob/master/public/Mini%20Exercises/miniEx5/RUNME.js)


**The concept of my work**
My work in this week's mini exercise represents both aspects of diversity and representation and data capturing. In my last week's mini exercise I focused on the aspect of data capturing 
that was 'labelling' on social media and defining people by these categories they are being put in on Facebook, Instagram, Twitter and other platforms. My work in this week takes these
same aspects but puts them in another context. When we talk about diversity and representation of minorities, it's often seen as being strong and proud of your identity and community created
around this specific category as a minority. Standing up against the social norms or going against the current of expectations for you as a person and your life. And it is brave and tough
to walk proud, cause what people often forget is why the minorities need to speak up and let the rest of the world know that they are there and just as valid as the others. 


The reason these people have the need to celebrate who they are is because they weren't celebrated by the rest to begin with. My work presents the thought of diversity but especially focuses
on sexuality and genderexpressions as a part of the LGBTQ+. When a user wants to show the world how proud they are of being themselves in today's society, they would probably want to show their 
relationshipstatus online as many other users outside of the minority do. They want to relate to the rest by sharing who they are, but the respons isn't always the same as when a heterosexual
couple announce their relationsship online. People in the norm of society still hate on others who don't fit into their expectations or ideologies, and that goes for identity as well. Queer 
sexuality is something that's being celebrated by many, but these same people might still have a problem with transgender men and women, or the idea of non conforming genders. So by expressing
yourself online as a person of these minorities that LGBTQ+ covers, you ultimately put yourself on the spot of being someone's outlet for hate and rage against the community. 

The concept of my work is to show exactly this, by taking the emoji of a perosn standing bravely with a prideflag, celebrating their gender identity and sexuality, but turning it into this
dark place for the internet to swallow. With datacapturing happening on the internet, it's easy for people to hide behind anonymous masks, bashing on other users. As I expressed in my last 
miniEx it's great to be able to connect with people online and sometimes these datacapturing categories help doing that, but other times it's risky and only for people to abuse. 


**What have I changed?**
To be more specific, I have taken the emoji from my miniEx 2 that represents a latina queer person and combined it with my buttons and input from my miniEx 4. I sorted some of the categories
out by only taking the ones that deals with gender identity, sexuality and relationsships to make it more focused on this particular group or community. I also chose to add the spying eyes since
that's exactly what I imagine datacapturing to look like, if you were to put a face on it. With this the very happy-looking emoji that I created some weeks ago has turned into a dark
dystopic concept of being kept an eye on and making the emoji look fragile and put on the spotlight. 


**What is Aesthetic Programming?**
On another note, Aesthetic Programming is exactly this - a relation between programming and digital culture. Programming being used as an artform or material for creating conceptual art, touching
on political, cultural and sociological subjects in the world we live in. Often being used as a way of lookin critically at something already excisting and either thinking of way to improve
a concept or comment on it with a critical perspective put completely on edge making it exaggerated to send a message. Software and programming is gradually becoming a bigger part of our world
and these artistic materials are learning more and more towards software- or digital art. With this graduate development of software, it also becomes more important for generations of people to
have or gain some knowledge of these phenomenons - being able to think in software as well as the common spoken language. 




