



let inputg; // variable for putting text into the program
let gender; // submit button for gender
let genderQ; // the question for gender

let inputs; // variable for putting text into the program
let sexuality; //submit button for sexuality
let sexualityQ; // the question for sexuality

let inputr; // variable for putting text into the program
let relationship; // submit button for relationship status
let relationshipQ; // the question for relationship status

var eye1 = {
  x: 100,
  y: 70,
  xspeed: 1
}

var eye2 = {
  x: 190,
  y: 70,
  xspeed: 1
}

function setup() {
  createCanvas(1000, 1000);
  frameRate(20); // decreasing the framerate

  inputg = createInput(); //creating the input function
  inputg.position(700, 150); // defining the position of the field of inputg

  gender = createButton('submit'); // creating the submit button for the gender question
  gender.position(inputg.x + inputg.width, 150); // defining the position of the submit button next to inputg
  gender.mousePressed(genderLabel); // declaring a mousePressed function in setup

  genderQ = createElement('h2', 'what is your gender?'); // asking the question as an element
  genderQ.style('color', '#ff0000'); // making the typography red
  genderQ.position(700, 100); // defining the position
  // gender data

  inputs = createInput();
  inputs.position(200, 350);

  sexuality = createButton('submit');
  sexuality.position(inputs.x + inputs.width, 350);
  sexuality.mousePressed(sexualityLabel);

  sexualityQ = createElement('h2', 'what is your sexuality?');
  sexualityQ.style('color', '#ff0000');
  sexualityQ.position(200, 300);
  // sexuality data

  inputr = createInput();
  inputr.position(150, 830);

  relationship = createButton('submit');
  relationship.position(inputr.x + inputr.width, 830);
  relationship.mousePressed(relationshipLabel);

  relationshipQ = createElement('h2', 'what is your relationship status?');
  relationshipQ.style('color', '#ff0000');
  relationshipQ.position(150, 780);
  // relationship data
}

function draw() {
  ellipseMode(CENTER);
  background(0);
  strokeWeight(2);
  stroke('red');
  noFill();
  ellipse(100,70,50,60);
  ellipse(100,70,60,70);
  ellipse(190,70,50,60);
  ellipse(190,70,60,70);
  // the static part of the eyes

  move(); // declaring a function in setup
  bounce(); // -||-
  display(); // -||-


  rectMode(RADIUS);
  noStroke();
  fill(94,8,107);
  rect(600,380,47,100,5,5,5,5);
  // hair

  rectMode(RADIUS);
  fill(189,141,85);
  rect(600,340,8,20);
  // the neck

  ellipseMode(RADIUS);
  fill(210,156,95);
  ellipse(600,300,40,40);
  // the head

  rectMode(RADIUS);
  fill(138,11,158);
  rect(600,270,47,25,100,100,5,5);
  fill(210,156,95);
  triangle(610,300,620,300,615,280); // chip in bangs
  // bangs

  stroke(189,141,85);
  strokeWeight(20);
  line(578,723,578,730); //left ancle
  line(622,723,622,730); //right ancle
  rectMode(CENTER);
  fill(14,18,18);
  noStroke();
  rect(578,745,27,25,5,5,0,0);
  rect(622,745,27,25,5,5,0,0);
  // shoes

  ellipseMode(RADIUS);
  fill(36,46,47);
  ellipse(600,528,40,35); //waist
  rectMode(CENTER);
  rect(578,623,35,200,20,20,5,5); // left leg
  rectMode(CENTER);
  rect(622,623,35,200,20,20,5,5); // right leg
  // the legs

  rectMode(CENTER);
  fill(42,137,144);
  rect(600, 435, 70, 150, 20, 20, 5, 5);
  // upper body

  fill(255);
  beginShape();
  vertex(470,150);
  vertex(560,30);
  vertex(665,70);
  vertex(580,180);
  endShape(); //base of the flag
  strokeWeight(25);
  stroke(255,0,0);
  line(589,174,669,78); // red
  stroke(255,137,0);
  line(569,168,649,69); // orange
  stroke(255,255,0);
  line(547,162,627,60); // yellow
  stroke(0,255,0);
  line(525,155,605,54); // green
  stroke(0,0,255);
  line(503,150,583,46); // blue
  stroke(162,0,255);
  line(484,143,561,38); // purple
  stroke(74,43,17);
  strokeWeight(5);
  line(470,150,820,250); //handle
  // pride flag

  stroke(210,156,95);
  strokeWeight(10);
  line(567,490,578,500); // hand
  stroke(42,137,144);
  strokeWeight(15);
  line(575,370,520,430); //shoulder to elbow
  stroke(42,137,144);
  strokeWeight(15);
  line(520,430,567,490); //elbow to wrist
  // left arm

  stroke(210,156,95);
  strokeWeight(10);
  line(665,220,665,205); // hand
  stroke(42,137,144);
  strokeWeight(15);
  line(628,375,660,300); //shoulder to elbow
  line(660,300,665,220); // shoulder to wrist
  // right arm
}

function genderLabel() {
  const gender = inputg.value();
  genderQ.html(gender);
  inputg.value('');
  // after submitting, the answer is viewed on the screen and the input is reset
}

function sexualityLabel() {
  const sexuality = inputs.value();
  sexualityQ.html(sexuality);
  inputs.value(''); // after submitting, the value resets
}

function relationshipLabel() {
  const relationship = inputr.value();
  relationshipQ.html(relationship);
  inputr.value(''); // after submitting, the value resets
}

  function move() {
    eye1.x = eye1.x + eye1.xspeed;
    eye2.x = eye2.x + eye2.xspeed;
    // making the pupils in the eyes move in the x-axis
  }

  function bounce() {
    if (eye1.x > 120 || eye1.x < 80) {
      eye1.xspeed = eye1.xspeed * -1;
    }

    if (eye2.x > 210 || eye2.x < 170) {
      eye2.xspeed = eye2.xspeed * -1;
    }
    // defining which x-positions the pupils can move in - and if they reach the end, they bounce back
  }

  function display() {
    stroke('red');
    noFill();
    ellipse(eye1.x, 70, 10, 20);
    ellipse(eye2.x, 70, 10, 20);
    // drawing the pupils
  }
