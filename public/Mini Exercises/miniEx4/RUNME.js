// In this program I chose to play around with the button function by making an artwork
// that represents the idea of internet platforms that captures the user's data - This all
// being rather personal information. The scanning eyes represent the internet capturing
// your data and watching your every move. Cause everything you do is useful data to them.
// Write something in the field and submit to answer the questions asked, and watch the
// platform categorizing you by using the labels to know who they're dealing with.
// Made by Clara Jassan.

let inputg; // variable for putting text into the program
let gender; // submit button for gender
let genderQ; // the question for gender

let inputs; // variable for putting text into the program
let sexuality; //submit button for sexuality
let sexualityQ; // the question for sexuality

let inputn; // variable for putting text into the program
let nationality; // submit button for nationality
let nationalityQ; // the question for nationality

let inputr; // variable for putting text into the program
let relationship; // submit button for relationship status
let relationshipQ; // the question for relationship status

let inputf; // variable for putting text into the program
let friends; // submit button for friends
let friendsQ; // the question for friends

var eye1 = {
  x: 100,
  y: 70,
  xspeed: 1
}

var eye2 = {
  x: 190,
  y: 70,
  xspeed: 1
}

function setup() {
  // create canvas
  createCanvas(1000, 1000);
  frameRate(20); // decreasing the framerate

  inputg = createInput(); //creating the input function
  inputg.position(650, 100); // defining the position of the field of inputg

  gender = createButton('submit'); // creating the submit button for the gender question
  gender.position(inputg.x + inputg.width, 100); // defining the position of the submit button next to inputg
  gender.mousePressed(genderLabel); // declaring a mousePressed function in setup

  genderQ = createElement('h2', 'what is your gender?'); // asking the question as an element
  genderQ.style('color', '#ff0000'); // making the typography red
  genderQ.position(650, 50); // defining the position
  // gender data

  inputs = createInput();
  inputs.position(300, 300);

  sexuality = createButton('submit');
  sexuality.position(inputs.x + inputs.width, 300);
  sexuality.mousePressed(sexualityLabel);

  sexualityQ = createElement('h2', 'what is your sexuality?');
  sexualityQ.style('color', '#ff0000');
  sexualityQ.position(300, 250);
  // sexuality data

  inputn = createInput();
  inputn.position(600, 450);

  nationality = createButton('submit');
  nationality.position(inputn.x + inputn.width, 450);
  nationality.mousePressed(nationalityLabel);

  nationalityQ = createElement('h2', 'what is your nationality?');
  nationalityQ.style('color', '#ff0000');
  nationalityQ.position(600, 400);
  // nationality data

  inputr = createInput();
  inputr.position(350, 680);

  relationship = createButton('submit');
  relationship.position(inputr.x + inputr.width, 680);
  relationship.mousePressed(relationshipLabel);

  relationshipQ = createElement('h2', 'what is your relationship status?');
  relationshipQ.style('color', '#ff0000');
  relationshipQ.position(350, 630);
  // relationship data

  inputf = createInput();
  inputf.position(100, 500);

  friends = createButton('submit');
  friends.position(inputf.x + inputf.width, 500);
  friends.mousePressed(friendsLabel);

  friendsQ = createElement('h2', 'who are your friends?');
  friendsQ.style('color', '#ff0000');
  friendsQ.position(100, 450);
  // friends data
}

function draw() {
  background(0);
  strokeWeight(2);
  stroke('red');
  noFill();
  ellipse(100,70,50,60);
  ellipse(100,70,60,70);
  ellipse(190,70,50,60);
  ellipse(190,70,60,70);
  // the static part of the eyes

  move(); // declaring a function in setup
  bounce(); // -||-
  display(); // -||-
}

function genderLabel() {
  const gender = inputg.value();
  genderQ.html(gender);
  inputg.value('');
  // after submitting, the answer is viewed on the screen and the input is reset
}

function sexualityLabel() {
  const sexuality = inputs.value();
  sexualityQ.html(sexuality);
  inputs.value(''); // after submitting, the value resets
}

function nationalityLabel() {
  const nationality = inputn.value();
  nationalityQ.html(nationality);
  inputn.value(''); // after submitting, the value resets
}

function relationshipLabel() {
  const relationship = inputr.value();
  relationshipQ.html(relationship);
  inputr.value(''); // after submitting, the value resets
}

function friendsLabel() {
  const friends = inputf.value();
  friendsQ.html(friends);
  inputf.value(''); // after submitting, the value resets
}

function move() {
  eye1.x = eye1.x + eye1.xspeed;
  eye2.x = eye2.x + eye2.xspeed;
  // making the pupils in the eyes move in the x-axis
}

function bounce() {
  if (eye1.x > 120 || eye1.x < 80) {
    eye1.xspeed = eye1.xspeed * -1;
  }

  if (eye2.x > 210 || eye2.x < 170) {
    eye2.xspeed = eye2.xspeed * -1;
  }
  // defining which x-positions the pupils can move in - and if they reach the end, they bounce back
}

function display() {
  stroke('red');
  noFill();
  ellipse(eye1.x, 70, 10, 20);
  ellipse(eye2.x, 70, 10, 20);
  // drawing the pupils
}

// References used:
// https://p5js.org/examples/dom-input-and-button.html
// https://www.youtube.com/watch?v=wRHAitGzBrg&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=20
