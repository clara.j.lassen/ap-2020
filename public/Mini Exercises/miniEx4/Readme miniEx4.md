**SHOW YOURSELF**

![screenshot](datacapturing.png)

[Link to miniEx4](https://clara.j.lassen.gitlab.io/ap-2020/Mini%20Exercises/miniEx4/)

[Link to repository](https://gitlab.com/clara.j.lassen/ap-2020/-/blob/master/public/Mini%20Exercises/miniEx4/RUNME.js)


**About the program**
The title for my artwork is "SHOW YOURSELF" and is a representation of the culture around oversharing whilst also being a part of data capturing on internet platforms. 
The artwork requests the user to submit information about themselves by answering a couple of personal questions. These being about their gender identity, social circle and thereby
social hierarchy, sexuality, nationality, and romantic relationship status. Beside the questions, the artwork visualizes the platform watching you with two eyes looking suspiciously
back and forth. Both the concept of oversharing and hereby the culture of labeling is something the intention of my artwork focuses on. But without putting the blame mainly on the user,
it also puts the internet "surveillance" in perspective of how our data is always being captured and used to identify us as users everywhere we go online. When answering the questions, the
program will go on to labeling the user as being their answer/ the information or data they gave away. It reduces them to a category.

**How is the program built?**
To code my artwork I have played around with the syntax for buttons and challenged the programs interactivity. My intention was to create a program that puts a darker light on the
use of social platforms - without meaning I'm not also at fault when using these functions and labels to create my identity on these platforms. When working with traditional art or 
digital paintings, the goal is almost always to make the user *think*. To work with a concept and put it into a new *perspective*. So when working with programming as my material for
an artwork I like to start out by sketching the idea I have in my mind in my regular material - this time being a digital drawing of what I'm imagining - and then I try to translate 
the idea into JavaScript. Here's the sketch I made for the mini exercise 4:

![screenshot](miniEx4sketch.png)

When translating the idea into the code editor, I used a reference for creating this input/button function (read repository). I started off by declaring my variables for both input, the 
button and the text element for the question/label. I do this for each of the questions/ categories, and then declare two variables for the pupils of the eyes. In my function setup() I
create an input function and then next to it a button to submit the answer. If the button is pressed the program will lead to the function "___Label" (like "genderLabel" or "nationalityLabel").
This function ___Label decides what's gonna happen once the button is pressed, where the questions are being changed to showing the answer instead ("genderQ.html(gender);") and then resets the
input to being blank ("inputg.value('');").
This is done for each of the categories. To make the pupils move I defined an area inside the eyes where they would be allowed to bounce back and forth, and made them move on the x-axis 
("if (eye1.x > 120 || eye1.x < 80) {
    eye1.xspeed = eye1.xspeed * -1; 
    }")

As my digital sketch shows, my intention was to make multiple eyes appear as the program kept running, and for this I wanted to make a class for the eyes, but wasn't able to figure out, how 
exactly I would make this group of objects duplicate as a *group*. This is something I would like to practice and gain more knowledge about when working with future mini exercises in AP. 


**A society full of data**
We live in a society where oversharing on social platforms is a common phenomenon. The reason for this may vary for the individual user, but often - especially when 
looking at young people's behaviors online - it's a way to feel understood and accepted for you are or who you present yourself to be. But this culture of oversharing often contributes 
to the culture of labeling and categorizing people based on the information they give about themselves - often being more personal or intimate knowledge. Something you wouldn't share with 
just anyone. But the user is far from the only one at fault for doing so. The social platforms feast upon all the data they are being given from the users, whether it's the user answering
questions about themselves or displaying their behavior online without knowing it. Everything you do on the internet can be tracked in some form of data, which is often being used by
big companies to gain knowledge about their users' actions or behaviors on a website. And for the most part people don't notice it, only to keep participating in this infinite system of
user-data. The red eyes in my artwork is an example of both this online surveillance and a scare factor for the user, as a reminder that somebody is collecting their information.


I'm personally tired of knowing that companies use my data without my actual knowledge to target ads at me or to label me as a certain user. It shouldn't be necessary for companies to know which
gender I identify as or what my sexuality is. My nationality shouldn't be their concern either as long as I get to pick my chosen language on the platform. But we feed them the information anyway, and why?
because we want to feel understood and accepted. It's an easy way for other people to quickly learn something about you that you want them to know, without having to explain yourself every time. But as I 
said, it only feeds the concept of putting people in a box and isolating them as being their options on Facebook or Instagram. You may feel like you're in control when putting in the information about 
yourself, but in the meantime the companies are using and selling the information about your identity by knowing what you like, when you like, who you like and how they're gonna get your attention - and thereby
get a better selling-rate. 



