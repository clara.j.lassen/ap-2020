

var joints = 5;
var lineLength = 100;
var speedRelation = 2;
var center;
var pendulumPath;
var angle = 0;
var maxAngle = 360;
var speed;
// standard value for the parameters

var showPendulum = true;
var showPendulumPath = true;


function setup() {
  createCanvas(windowWidth, windowHeight);
  colorMode(HSB, 360, 100, 100, 100);
  noFill();
  strokeWeight(1);

  center = createVector(width / 2, height / 2); // starts the drawing in the center
  startDrawing(); // defines a function
}


function startDrawing() {
  pendulumPath = [];  // new empty array for each joint
  for (var i = 0; i < joints; i++) {
    pendulumPath.push([]);
  }

  angle = 0;
  speed = (8 / pow(1.75, joints - 1) / pow(2, speedRelation - 1));
}


function draw() {
  background(0);
  angle += speed;

  if (angle <= maxAngle + speed) { // each frame, create new positions for each joint
    var pos = center.copy(); // start at the center position

    for (var i = 0; i < joints; i++) {
      var a = angle * pow(speedRelation, i);
      if (i % 2 == 1) a = -a;
      var nextPos = p5.Vector.fromAngle(radians(a));
      nextPos.setMag((joints - i) / joints * lineLength);
      nextPos.add(pos);

      if (showPendulum) { // if the pendulum is being shown, it starts to draw the center
        noStroke();
        fill(0, 10);
        ellipse(pos.x, pos.y, 4, 4);
        noFill();
        stroke(0, 10);
        line(pos.x, pos.y, nextPos.x, nextPos.y);
      }

      pendulumPath[i].push(nextPos);
      pos = nextPos;
    }
  }


  if (showPendulumPath) {  // draws the path for each joint/lines
    strokeWeight(1.6);
    for (var i = 0; i < pendulumPath.length; i++) {
      var path = pendulumPath[i];

      beginShape();
      var hue = map(i, 0, joints, 120, 360);
      stroke(hue, 100, 100, 100);
      for (var j = 0; j < path.length; j++) {
        vertex(path[j].x, path[j].y);
      }
      endShape();
    }
  }
}

function keyPressed() {

  if (keyCode == DELETE || keyCode == BACKSPACE) startDrawing(); // resets the drawing

  if (keyCode == UP_ARROW) { // increases the length of lines
    lineLength += 2;
    startDrawing();
  }
  if (keyCode == DOWN_ARROW) { // decreases the length of lines
    lineLength -= 2;
    startDrawing();
  }
  if (keyCode == LEFT_ARROW) { // decreases the amount of joints
    joints--;
    if (joints < 1) joints = 1;
    startDrawing();
  }
  if (keyCode == RIGHT_ARROW) { // increases the amount of joints
    joints++;
    if (joints > 10) joints = 10;
    startDrawing();
  }

  if (key == '+') { // increases the speedRelation
    speedRelation += 0.5;
    if (speedRelation > 5) speedRelation = 5;
    startDrawing();
  }
  if (key == '-') { // decreases the speedRelation
    speedRelation -= 0.5;
    if (speedRelation < 2) speedRelation = 2;
    startDrawing();
  }
}
