![screenshot](screenshot_miniEx7.png)

[MiniEx7](https://clara.j.lassen.gitlab.io/ap-2020/Mini%20Exercises/miniEx7/)

[Repository](https://gitlab.com/clara.j.lassen/ap-2020/-/blob/master/public/Mini%20Exercises/miniEx7/sketch.js)


## About the program
My work for this week is a program that generates art automatically, and can be customized by the user by changing the parameters by using specific keys on the computer. Due to a lot of personal changes this past week (here amongst moving in the middle of the global pandemic), I have had to compromise my work on the mini exercise a lot. My program this week is, for this reason, heavily inspired by the artwork “P_2_2_6_01” by Hermann Schmidt (2018). The way I chose to approach the task this week is therefore more focused on understanding the concept of automated generators and how it influences the program for the user experience - referring the experience to the required readings for class 8.

When I first looked at the mini exercise for this week, I immediately thought about an idea where I would generate activities and ideas for people to do at home in this critical time in our society. Due to the Covid-19 virus we all need new ways to think about routine and keeping ourselves active in our homes, practicing social distancing, which very easily can become boring. But I quickly realised that this wouldn’t be as automated as the theme for this week suggests. It would require that I made the program create ideas by itself instead of plotting an array of a few things to do into the program. I then chose to work with art as an auto generated concept. To visualize this idea I made a sketch of how I wanted different colors and patterns to meet:

![screenshot](sketch_miniEx7.PNG)


The program creates a pattern by connecting lines and joints in different colors. This program is a lot like the danish kid’s toy “Krusedulle Kim”, but made digital. The toy draws patterns/ mandalas automatically, making it possible to customize by adjusting the ‘arms’ on the toy and changing the markers. 
The rules in this program are in some ways similar:

* When the program begins, an ellipse is being drawn around the center with a line. When this line reaches the beginning of the circle, the program ends and the drawing is done.
* Each set of joints/lines have a standard value, resulting in a ‘standard drawing’. if the parameters are changed, more or less joints will appear, the speed will increase or decrease and the length of the lines will also increase or decrease.
*   “-” decreases the speed relation
*   “+” increases the speed relation
*   arrow down decreases the length of lines
*   arrow up increases the length of lines
*   arrow left decreases joints
*   arrow right increases joints. 
*   delete and backspace clear the screen and draw the same drawing from the beginning. 

The role of rules in this program give the joints their own individual instructions as to how they should compute and generate an image with patterns in different colors. When the user change up the parameters in the program they will hereby recieve an unexpected result with a new image. Just as the “Krusedulle Kim” the user won’t know exactly how it will turn out before the drawing is done. Given that the lines that create the drawing are not controlled by the user, it makes them seem like they move freely without any limitations even though they are computed and have a very specific set of instructions for the program, ultimately resulting in a pattern. 

## Program related to the literature
In the chapter “Randomness” by Monfort, N. (2012), the author talks about what effect the unknown and unexpected has on us and how come people develop an addiction in the gambling world, also referred to as the “Game of Chance”. As Monfort quoted Anatole France saying that “gambling is a hand-to-hand encounter with Fate… The allure of gambling… rests on uncertainty. Uncertainty is so compelling that even otherwise skill-based games usually incorporate formal elements of chance” - (Monfort 2012:122).
We humans love the thrill of taking chances and not knowing what to expect, resulting in either being pleasantly surprised or disappointed, which in general plays a big role when gambling. But we meet these aspects of gambling everywhere in our everyday lives. When we take chances that might be a little risky or when we simply have no idea what to expect. 
This phenomenon is also seen in a lot of artist’s lives when commiting to an art piece. The small choices as well as the critical for the painting, drawing, recipe or whatever the medium for the artist is. How this program handles this concept is a way to fix it as if it was an issue, but to let the artist take a step back and let fate do its thing. The user won’t have to think to hard or struggle just to express themselves creatively. This program is a perfect way to be creative and play with fate in a way that won’t end with bigger consequences. 

Inspiration:
http://www.generative-gestaltung.de/2/sketches/?01_P/P_2_2_6_01
