
class Text {
  constructor(x, y, t) {
    this.x = windowWidth/2;
    this.y =y;
    this.story = t;
  }

  move() {
    this.y-=1; // changes the y-axis with each frame to move the text upwards 
  }

  show() {
    fill(146,0,0);
    textAlign(CENTER);
    textStyle(BOLD);
    textSize(80);
    text(this.story, this.x, this.y);
    // styles the text
  }
}
