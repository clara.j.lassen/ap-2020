var data;
var sound;
var story;
var object = [];

function preload() {
  data = loadJSON("LRR.json");
  sound = loadSound("miniex8_sound.mp3");
  clouds = loadImage("clouds.jpg");
}


function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(30); // sets the frames/ sec down to make the lines move slower

  story = data.story; // names the data from the json file as story
  sound.play(); // plays the Little Red Riding Hood when the program starts

  for (i = 0; i < story.length; i++) { // makess a new object for every line in the story. Saves the text as a string
    object[i] = new Text(50, 200*i+100, story[i]);
  }
}


function draw() {
  background(255);
  image(clouds, 20, 20); // displays the image and defines the position
  print(object.length);

  for (var i = 0; i < object.length; i++) {
    object[i].move(); // calls the object's movement from class Text
    object[i].show(); // calls the object's display from class Text
  }
}
