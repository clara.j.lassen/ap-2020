## "Sexting", by Anne E. and Clara

![screenshot](screenshot_miniEx8.png)

[Mini exercise 8](https://clara.j.lassen.gitlab.io/ap-2020/Mini%20Exercises/miniEx8/)

[Repository](https://gitlab.com/clara.j.lassen/ap-2020/-/tree/master/public/Mini%20Exercises/miniEx8)

**Disclaimer**

Since our program involves audio we would suggest that you have your sound turned on.


**Title and description of our work**

We have chosen to call our work Sexting. To those who may not be familiar with this term it essentially means to send text messages with sexual content. This is what we intended to do with our choice of text in the program. We wanted to twist the traditional way of telling the popular children’s story, because we discovered that it has some very dark and vulgar elements to it. In a lot of the popular tales intended mainly for children there are messages of a sexual or in other ways inappropriate nature. But they are hidden or censured in a way and you have to look for them to notice. We feel that this illustrates a core problem in the programming industry and many other media industries. The sender/maker has a lot of influence on the way the material is perceived. Our main intention with this program is to highlight the power relation between sender and receiver and also to display the impact a text can have on the senses in both its written and spoken form.  


**How does the program work and what did we use and learn?**

Our program has 4 global variables that control the sound input (the story about the Little Red Riding Hood being read), the data from our json file with sentences from the original story, and defining these sentences as objects in an array. 

When running the program the story will be read aloud by a woman, in a happily manner. While the story is being read, carefully picked sentences from the story is being shown on the screen. They’re slowly moving upwards like credits in a movie, to allow the user to read these while listening to the story. To make the objects move, we created a class called “Text”. In our move function in this class we used the syntax “this.y-=1;” to make it move upwards for each frame. 

To the left of the screen a sweet kid’s illustration is being shown to create an even bigger contrast between the two kinds of “voices”. 

When working with this week’s mini exercise, we got to play around with json files and used this format by storing the parts of the story we picked out, sentence by sentence. We call these into the sketch program by loading the json file as “data” and then calling path of the data in the json file as story. To make each of the sentences into objects, we used a for-loop to go through each of them: “for (i = 0; i < story.length; i++) { object[i] = new Text(50, 200*i+50, story[i]);}”. The parameters of this object are x, y, and a story array.Y implicates both the distance between each sentences and where they should start at the beginning of the program. 

Some of the syntax we used in this week’s mini exercise is inspired by the way we coded games in mini exercise 6, having objects going through arrays, along with figuring out how to use classes and for-loops. What both of us learned from this week’s mini exercise through trying out syntax we weren’t quite confident with, was trying to make sense of for-loops and playing around with different inputs in the program (this being sound and json files). We also noticed that it’s remarkably easier to get through struggles with the code and coming up with ideas for a program when you’re two people working on the same project. 


**Analysis**

As suggested in the text “The Aesthetics of Generative Code” by Geoff Cox, ALex McLean and Adrian Wards (2001), multimedia might play a role in binding together different arts and senses. What Aristotle once called “common sense” was not seen as a sixth sense but rather an “operating system” (Cox et al., 2001: 2).

By playing around with both an audio input and written text we feel that the senses are bound together as suggested in “The Aesthetics of Generative Code”. A tone or atmosphere is set by the controversial play between listening to a merry woman's voice and looking at large, blood-red and quite odd sentences written in caps. The “organising mechanism” is put to work here (Cox et al., 2001: 2) and the program is experienced in a distribution between the senses - though mainly through sight and hearing. 

To grasp the understanding of voices and code language, we find it almost necessary to incorporate multiple artforms and senses, which is also what we’ve been asked to reflect upon in this week’s mini exercise. As it’s also stated in the text that “It is now generally accepted that sense perception alone is simply not enough unless contextualised within the world of ideas” - (ibid).

In “The Aesthetics of Generative Code” it is described how aesthetics in code does not lie only in the written form but a big part of it is also in its execution. In terms of our code our work only comes “alive” with the execution of it. It has to be put in connection with human senses or the idea and purpose falls apart. 

**Final notes**

One last remark on this program is the choice of the aesthetic expression of our text. We wanted it to give anything BUT the cozy and innocent impression left by the sort of motherly voice reading the traditional story out loud. This is why we chose the style of the text to be big letters written in bold and caps. And since this is a very violent story we chose a red colour in a nuance people associate with blood.


