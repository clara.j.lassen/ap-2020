class Trash {
  constructor() {
    // initialize the object's properties
    this.speed = floor(random(2,5));
    this.pos = new createVector(width+5, random(12,height/1.7));
    this.size = random(15,35);
  }

  move() {
    this.pos.x -= this.speed;  // move the object across the screen
  }

  show() {
      image(trash, this.pos.x ,this.pos.y, this.size, this.size); // show the object as the fish
  }
}
