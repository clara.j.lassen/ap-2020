let chonkySize = {
  w: 250,
  h: 170
};
let chonky; // the turtle. Chonky is his name
let chonkyPosY;
let mini_height; // the min. height for the turtle


let blob = []; // the fishes
let min_blob = 5; // minimum fishes on the screen

let trash = []; // the garbage
let min_trash = 5; // minimum trash on the screen

let score = 0, lose = 0; // the score of the fish and trash eaten

function preload() { // load the graphics
  chonky = loadImage("data/turtle.gif");
  blob = loadImage("data/fish.gif");
  trash = loadImage("data/trash.gif");
}

function setup() {
  createCanvas(1000,600);
  chonkyPosY = height/2;
  mini_height = 400; // sets value for minimum height

 for (let i=0; i<=min_blob; i++) {
   blob[i] = new Blob(); // construct a new object instance
 }

 for (let i=0; i<=min_trash; i++) {
   trash[i] = new Trash(); // construct a new object instance
 }
}

function draw() {
  background(100,131,255);
  noStroke();
  fill(236,216,161);
  rect(0,550,1000,50); // the sand in the background
  image(chonky, 20,chonkyPosY, chonkySize.w, chonkySize.h); // the turtle
  showBlob();
  checkBlobNum();
  showTrash();
  checkTrashNum();

  checkEating();
  displayScore();
  checkResult();
}

function showBlob() {
  for (let i = 0; i <blob.length; i++) {
    blob[i].move();
    blob[i].show();
    // calls the fishes in a loop
  }
}

function checkBlobNum() {
    if (blob.length < min_blob) {
      blob.push(new Blob());
      // checks the length of the fish array and pushes new fish to the program
      }
    }


function showTrash() {
  for (let i = 0; i <trash.length; i++) {
    trash[i].move();
    trash[i].show();
    // calls the trash in a loop
  }
}

function checkTrashNum() {
  if (trash.length < min_trash) {
    trash.push(new Trash());
    // checks the length of the trash array and pushes new trash to the program
  }
}

function checkEating() {
  // calculate the distance between each fish
  for (let i = 0; i <blob.length; i++) {
    let d = int(dist(chonkySize.w/2, chonkyPosY+chonkySize.h/2,blob[i].pos.x, blob[i].pos.y));
    if (d < chonkySize.w/2.5) {
      score++; // if the turtle eats the fish, add 1 to the score
      blob.splice(i,1);
    }
  }
  // calculate the distance between each trash
 for (let i = 0; i <trash.length; i++) {
    let d = int(dist(chonkySize.w/2, chonkyPosY+chonkySize.h/2,trash[i].pos.x, trash[i].pos.y));
    if (d < chonkySize.w/2.5) {
      lose++; // if the turtle eats the trash, add 1 to the lose
      trash.splice(i,1);
    }
  }
}

function displayScore() {
  textSize(20);
  text('You have eaten ' + score + " fish", width/2.3, height/2);
  text('You have eaten ' + lose + " trash", width/2.3, height/2+30);
  // display the score of fish and trash on the screen
}

function checkResult() {
  if (lose > 3) {
    textSize(30);
    text("You ate too much trash... GAME OVER", 400, 200 );
    noLoop();
    // check if the lose score is bigger than 3 and display the message
  }
}

function keyPressed() {
  if (keyCode === UP_ARROW) {
    chonkyPosY -= 40;
  } else if (keyCode === DOWN_ARROW) {
    chonkyPosY += 40;
    // changes the y position when the up and down arrows are being pressed
  }
  if (chonkyPosY > mini_height) {
    chonkyPosY = mini_height;
  } else if (chonkyPosY < 0 - chonkySize.w/4) {
    chonkyPosY = 0;
    // creates a barrier for the turtle's y position.
  }
}
