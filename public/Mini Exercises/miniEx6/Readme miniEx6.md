**Please read before trying the game**

![screenshot](screenshot.png)

[MiniEx6](https://clara.j.lassen.gitlab.io/ap-2020/Mini%20Exercises/miniEx6/)

[Repository](https://gitlab.com/clara.j.lassen/ap-2020/-/tree/master/public%2FMini%20Exercises%2FminiEx6)

## How does the game work?
In my program this week I made a game featuring an underwater environment where the player controls a seaturtle. The turtle is being moved on the y-axis by using the up- and down arrows. The goal is to eat as many fish as possible without eating or getting caught in any trash.
Everytime the turtle, Chonky, eats a fish, the score goes up by 1. However, if he eats trash the lose count goes up by 1. When 3 pieces of trash has been eaten, the game will send a message that the game is over.
When I got the idea for the game, I quickly sketched it digitally to know what my goal for the program was. The theme of my game is polution in the ocean, and with the game I hope to spread the message and 
awareness of this issue. Especially turtles are known for being exposed and hurt by plastic in the water, and so I wanted to make the message clear by taking exactly this issue and incoorperating it in my program.
Though this is a very tough and terrible issue across the world, I wanted to make the game cute and less graphic, by creating my own simple designs for the objects of the game - The turtle Chonky, the fish Blob and the trash, 
all specially made by my girlfriend.

Unfortunately I had a lot of issues with creating my game, and more specifically getting the fish and trash to appear - which both are a huge part of the whole game. I had multiple people - including a hobby-programmer - trying 
to fix the issue and whatever was not allowing the objects to show up, but after hours, no one were able to help. I tried to go through Winnie's source code from Class 7, since her game was something I took inspiration from, but 
with very little to no luck. I included my sketches and gifs of the objects to hopefully give a better understanding of my intention with the game.

![screenshot](sketch.PNG)

![](https://gitlab.com/clara.j.lassen/ap-2020/-/raw/master/public/Mini%20Exercises/miniEx6/data/turtle.GIF) ![](https://gitlab.com/clara.j.lassen/ap-2020/-/raw/master/public/Mini%20Exercises/miniEx6/data/fish.GIF) 
![](https://gitlab.com/clara.j.lassen/ap-2020/-/raw/master/public/Mini%20Exercises/miniEx6/data/trash.GIF)


## The code behind the objects
Looking at the syntax for my program, I started off by declaring my variables for the 3 different objects. The turtle: let chonkySize; (to resize the gif), let chonky;, let chonkyPosY; (to define the y-position of the turtle), let mini_height; (to prevent the turtle from moving 
out of frame). With the fish and the trash I defined the objects with let blob = []; and let trash = [];, hereby choosing the minimum number of objects active on the screen with let min_blob = 5; and the same for trash. In my function preload() I loaded the 3 gifs and in draw displayed
the turtle by image(chonky, 20, chonkyPosY, chonkySize.w, chonkySize.h); to define the placement of the turtle and his given size. The other two were displayed in each of their classes (class Blob + class Trash). In function draw() I call a lot of my functions, including showBlob(), checkBlobNum(),
checkEating() and displayScore() to display the text on the screen that reveals the score of both fish and trash. In my function showBlob I use a for loop to check the length of the blob array and thereby call both blob[i].move(); and blob[i].show();. These are being used in the class Blob to 
control the job of each fish active on the screen. In my function checkBlobNum() the program pushes a new fish/blob if the active fish are less than the minimum required. The exact same goes for the trash object.

The classes in my program consist of a constructor, move function and show function. The constructor initializes the object's properties like speed, position and size. The move function includes the syntax this.pos.x -= this.speed; which moves the object across the screen from right to left, towards
the turtle. The show function includes the image of the object, just like the turtle image in function draw(). 

## Object-oriented programming and abstraction
The aspects of programming we have worked with so far addresses certain discussions such as language, literacy and objects. We must understand why it is important to learn programming and what, amongst the infinite things, we want to use it for. In the text "The Obscure Objects of Object Orientation"
by Matthew Fuller and Andrew Goffey (2017), they mentioned the following:

>  *"It is with the invention of programming languages that the broad parameters of how a machine can talk to the outside world - to itself, to other machines, to humans, to the environment and so on - are initially established." - (Fuller & Goffey, 2017:2)*

It is how the machine talks to the rest of the world, that we interpret its language and messages being broadcasted to us, machines and itself. But it's different for every situation. The users playing the games see these objects as a part of the game and the method they will need to gain knowledge 
about in order to play the game as intented. But what they don't see is the construction behind these objects and how it makes the player able to interact with these objects and methods. These thoughts about construction are something the programmer has to work with in order to make the game run as intended
in the first place. When working with objects you generate instances of ideal types or classes. However, Fuller and Goffey mention that the "actual behavior is somethingthat arises from the messages passed from other objects, and it is the messages... that are actually of most importance."
(Fuller & Goffey, 2017:4)

With abstractions it is also stated by Fuller and Goffey that humans often take the interaction with machines for granted. This might be seen in situation where people use technology like smartphones or their computers, without actually giving a thought to how it's all possible. "How does the computer know 
exactly what it has to do, when I press this button?" "How come my smartphone answers to all the swipes I make on the screen, when I want do access a shortcut?". The answer to these questions is that a lot of programmers and software developers have worked with these abstraction and different objects. This 
is also being further clarified:

>  *"… the capture of agency links the formal structuring of computational objects to the broader processes of which they are a part, allowing us in turn (1) to specify more precisely that the formal structuring and composition of objects has a vector-link quality; and (2) to attend more directly to the
correlative feature of reterritorialization. Abstracting from, in this sense of a real process, is equally an abstracting to: an abstraction is only effective on condition that it forms part of another, broader set of relations, in which and by which it can be stabilized and fortified." - (Fuller & Goffey, 2017:6-7)*

## Connection between game and digital culture
The requirement for my program is that the user is somewhat familiar with the gamegenre sidescroller. They know how to operate the arrow keys, with a great signifier that the up-arrow makes the turtle move up and the down-arrow makes it go further down. The text in the middle of the screen gives the user somewhat of 
an idea about what to expect and to collect. But taking a step back from the program, it is also clear that the user would need to know at least something about the climate crisis in the ocean and in general around the world. "Yes trash is bad, but how bas is it really? Will the turtle die?". The game contains a 
political discussion about the well being of the climate and our earth, along with all the living species. But speaking about the technical abstractions of the program, the user would just know how to use the arrows. We see this in a lot of other games where the protagonist is being moved around by the user. 
Scroll-bars are also a great comparison to this method and signifier that shows "up and down". The user wouldn't need a lot of detailed instruction to know how to control the turtle. When we use scroll-bars on the computer, we're used to scrolling up and down the page, also controlling the position of the visible sections.

But a problem this method along with side-scrollers might face as the technology is still rapidly evolving into "screens only", is that many kids games today operate through tablets or smartphones and not as much the big websites with thousands of small games such as miniclip or y8. Kids will no longer be using the keyboard
as much, and therefore lose the understanding of the methods and operations being abstracted when playing games such as this one with a cute turtle, ready to be moved.


