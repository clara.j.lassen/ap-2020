Link to screenshot: https://i.imgur.com/mlte40v.png

Link to my work: https://clara.j.lassen.gitlab.io/ap-2020/Mini%20Exercises/miniEx1/

I started my miniEx1 with some prior knowledge of programming, which came to be a benefit for my understanding of the language used to code. 
My thought process was experimental since I didn't exactly know what I wanted to create, but went with it along the way.
First I started with simple shapes creating a landscape with trees and a snowman, but I also wanted to make the program interactive in some way.
I experimented with ways to draw snow with the mousecursor, using the "if mouseIsPressed" to draw ellipses illustrating snowflakes. 
This didn't quite satisfy me, so I tried looking through the reference and example pages on p5.js's website and found a script for simulating falling snowflakes. 
I modified the update time to go slower, so the snow would fall more idyllically.
Even though a big part of coding is taking already existing code and modifying it, I don't like to just copy and paste it into my own program and call it day.
I want to be able to understand the code as I type it into my program. 
So my experience with coding my first work in AP would be that I not only want to think in coding languages but also understand how other people have thought, and learn from that.
The coding experience is different from reading and writing texts in a way that I'm learning a new language with different syntax (or grammar I guess you could say, if you were to compare them to each other). 
Before being able to think freely in programming, you would have to have some basic understanding of the language, and that can be quite difficult in the early process of learning.
Programming is something I have been interested in for quite some time but never really got to learn about before having a programming class at my old gymnasium. 
The teacher wasn't very interested in teaching people with no prior experience and didn't offer any theory, which demotivated me for a long time.
I feared getting back into programming after completing the class, but the assigned readings for week 6 helped me get that motivation back. 
The texts made it clear that programming sincerely is a language to be learned and everyone should have the opportunity to learn.
Not just technology experts but students, designers, etc. as well. I need to know that the ability to code wasn't a gift you were born with, but something you learn
-from knowing nothing to spark an interest in the field and eventually being able to use the language.
