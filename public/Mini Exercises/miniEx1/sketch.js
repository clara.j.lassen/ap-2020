let snowflakes = [];

// array to hold the snowflake objects

function setup() {
  createCanvas(1200,600);

};

function draw() {
  background(2,0,60);
  let t = frameCount / 800; //update time
  for (let i = 0; i < random(5); i++) { //creates a random number of snowflakes for each frame
    snowflakes.push(new snowflake()); //adds the snowflake object
  }

  for (let flake of snowflakes) { //loops through snowflakes
    flake.update(t); //updates snowflake position
    flake.display(); //draws shows snowflakes
  }

  noStroke();
  fill(255);
  rect(0,500,1200,100);
  // draws the snow

  fill(61,37,15); {
  rect(50,350,40,200);
  // draws the trunk
  fill(28,75,13);
  rect(0,340,145,35);
  fill(28,75,13);
  rect(20,305,100,35);
  fill(28,75,13);
  rect(40,270,60,35);
  fill(28,75,13);
  rect(30,400,20,20);
  fill(28,75,13);
  rect(90,450,20,20);
  // draws leaves on the tree
  }

  fill(61,37,15); {
  rect(500,350,40,200);
  // another tree
  fill(28,75,13);
  rect(447,340,145,35);
  fill(28,75,13);
  rect(470,305,100,35);
  fill(28,75,13);
  rect(490,270,60,35);
  fill(28,75,13);
  rect(480,460,20,20);
  fill(28,75,13);
  rect(540,430,20,20);
  // more leaves
  }

  fill(61,37,15); {
  rect(950,350,40,200);
  // yep, it's another tree
  fill(28,75,13);
  rect(897,340,145,35);
  fill(28,75,13);
  rect(920,305,100,35);
  fill(28,75,13);
  rect(940,270,60,35);
  fill(28,75,13);
  rect(930,460,20,20);
  fill(28,75,13);
  rect(990,430,20,20);
  fill(28,75,13);
  rect(930,400,20,20);
  // even more leaves
  }

  stroke(0); {
  fill(255);
  ellipse(300,500,80,80);
  fill(255);
  ellipse(300,430,60,60);
  fill(255);
  ellipse(300,380,40,40);

  fill(0);
  ellipse(288,378,5,5);
  fill(0);
  ellipse(312,378,5,5);
  fill(255,145,0);
  ellipse(300,382,5,5);
  fill(0);
  ellipse(300,420,5,5);
  fill(0);
  ellipse(300,440,5,5);
  fill(0);
  ellipse(300,480,5,5);
  fill(0);
  ellipse(300,500,5,5);
  fill(0);
  ellipse(300,520,5,5);

  fill(61,37,15);
  rect(250,425,20,5);
  fill(61,37,15);
  rect(330,425,20,5);
  // draws snowman

  }

  noStroke();
  if (mouseIsPressed) {
  fill(255);
  ellipse(mouseX,mouseY,20,20);
  //snow follows the mouse when pressed
  }

}

function snowflake() { //snowflake class
  this.posX = 0;
  this.posY = random(-50,0);
  this.initialangle = random(0,2*PI);
  this.size = random(2,5);
  //initializes coordinates

  this.radius = sqrt(random(pow(width/2,2))); //choses radius so the snowflakes will spread out

  this.update = function(time) {
  let w = 0.6;
  let angle = w * time + this.initialangle;
  this.posX = width / 2 + this.radius * sin(angle);
  //x position follow a circle

  this.posY += pow(this.size, 0,5); //different size snowflakes fall at different y-speeds

  if (this.posY > height) {
  let index = snowflakes.indexOf(this);
  snowflakes.splice(index, 1);
  //delete snowflakes that pass the end of the screen
    }
  };

  this.display = function() {
    fill(255);
    ellipse(this.posX, this.posY, this.size);
    // creates the actual figure of the snowflake
  };
}
