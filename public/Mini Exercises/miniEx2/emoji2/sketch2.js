// This emoji represents an alien being alarmed by the western society on Earth and
// how white people think they can solve the problem with the lack of representation
// of the whole human species, alone. Made by Clara Jassan.

//PRESS THE MOUSE IN THE PROGRAM

function setup() {
  createCanvas(windowWidth,windowHeight);
}

function draw() {
  background(5,15,37);
  noStroke();
  fill(255,255,238);
  ellipse(150,120,80,80);
  noFill();
  strokeWeight(2);
  stroke(233,232,213);
  ellipse(130,110,20,20);
  ellipse(145,90,10,10);
  ellipse(150,150,15,10);
  ellipse(178,120,10,15);
  ellipse(155,130,5,5);

  noStroke();
  ellipseMode(CENTER);
  fill(90,0,255,);
  ellipse(500,400,300,300); // head
  fill(200);
  ellipse(430,400,80,120);
  ellipse(570,400,80,120); //eyes

  if (mouseIsPressed) {
    stroke(0);
    strokeWeight(6);
    line(450,365,450,430);
    line(590,365,590,430); //looking right
  } else {
    stroke(0);
    strokeWeight(6);
    line(410,365,410,430);
    line(550,365,550,430); //looking left
    }

  stroke(255);
  strokeWeight(6);
  line(450,370,470,370);
  line(590,370,610,370); // highlight in eyes. This layer is "above" the pupils

  if (mouseIsPressed) {
    fill(4);
    ellipse(490,450,30,30); //big mouth
  } else {
    fill(4);
    ellipse(490,450,20,20); //small mouth
  }

  if (mouseIsPressed) {
    stroke(90,0,255);
    strokeWeight(10);
    line(430,270,430,180); //left ear
    line(570,270,570,180); //right ear
    ellipseMode();
    noFill();
    ellipse(430,170,20,20); // left ear
    ellipse(570,170,20,20); //right ear
    // moves ears
  } else {
    stroke(90,0,255);
    strokeWeight(10);
    line(370,350,300,300); //left ear
    line(630,350,700,300); //right ear
    ellipseMode();
    noFill();
    ellipse(290,290,20,20); // left ear
    ellipse(710,290,20,20); //right ear
    // standard position of the ears
  }

  if (mouseIsPressed) {
    noFill();
    stroke(0);
    strokeWeight(5);
    curve(370,360,410,300,450,300,490,360); // left eyebrow
    curve(510,360,550,320,590,320,630,360); // right eyebrow
    // raises the left eyebrow
  } else {
    noFill();
    stroke(0);
    strokeWeight(5);
    curve(370,360,410,320,450,320,490,360); // left eyebrow
    curve(510,360,550,320,590,320,630,360); // right eyebrow
    // standard position of the brows
  }

}
