![screenshot](miniEx2_emoji1.png) ![screenshot](miniEx2_emoji2.png)

[Link to emoji 1](https://clara.j.lassen.gitlab.io/ap-2020/Mini%20Exercises/miniEx2/emoji1/)

[Link to emoji 2](https://clara.j.lassen.gitlab.io/ap-2020/Mini%20Exercises/miniEx2/emoji2/)

(If the links don't work, please copy the code from my sketch file into either Atom or the online editor on P5.js's website :))


[Link to repository for emoji 1](https://gitlab.com/clara.j.lassen/ap-2020/-/tree/master/public/Mini%20Exercises/miniEx2/emoji1)

[Link to repository for emoji 2](https://gitlab.com/clara.j.lassen/ap-2020/-/tree/master/public/Mini%20Exercises/miniEx2/emoji2)


**What my program is about and what I have learned**
My initial idea with creating two emojis was to create one that showed representation for a minority that is rarely talked about, and to create one as a contrast of this - reflecting
on the assigned text "*Modifying the Universal*". My first emoji is a static piece illustrating a queer latina in an lgbt parade. A big part of the syntax I used for this emoji was
shapes and geometric lines. As an artist I wanted to play around with the 2D shapes and try to create a piece of art similar to what I would normally paint digitally. To properly
imagine what I wanted my emoji to look like, I drew a quick sketch of the idea I had in my head, and from there I just had to figure out how I translated this into code. I quickly
figured out how difficult the pose I chose for the girl was, so I changed it from a profile point-of-view to a front-view. The hard part for me was to translate the natural strokes
of the drawing to coordinates in the program, but I was rather still in my comfortzone of my experience with programming doing this piece. I played around with some colors and learned
how to create new shapes I hadn't used before. 

But with my other emoji I wanted to do something different. The second emoji illustrates an alien from outer space - based on my huge love and fascination for space and aliens - 
which I wanted to make interactive. This time I didn't draw a sketch before creating the coded sketch, and wanted to see where it took me. So I experimented around with shapes and 
new features that would change with the event "if (mouseIsPressed)". I created a simple static background for the alien to put it in a 'non-earth' environment, in hopes that it would 
read better for the reader (if my Shrek looking alien didn't give it away). The point of this emoji is to make it look shocked or alarmed by something. And something being humans, but 
especially one specific group of people. After creating the final sketch I wanted to draw a quick doodle of the alien as well, and here is a picture of both of them:

[Link to sketches](https://imgur.com/BXMQAxP)


**My emojis in a cultural context concerning representation**
As a lesbian latina myself, I wanted my first emoji to highlight the issues and taboos of the society in Mexico, when it comes to queer sexuality. It's difficult enough to openly
talk about your sexuality without worrying how people outside the LGBT community will respond, but especially in conservative, catholic countries where the consequenses can change 
your life - not nessesarily in an eyeopening, positive way. Many young latinx struggle to come out to their family and friends, because ultimately they don't know if they will get
kicked out of their homes and disowned by their family the next day. It's a tough conversation and situation to confront, which may be a reason for many to stay in the closet until 
they truly start their own lives. But not only conservative countries lack the representation and mention about sexuality. So does big companies with big voices. Voices to have a huge
impact on both youths and society in general. As an example you could look at Apple's strict protocol on what's acceptable and what's not. Yes they created the queer couple emoji, 
but they still refuse to introduce sexuality in their content. It's companies like these - they have the voice to send a message to the young people, afraid of talking abuot their
sexualities - who avoids these political debates by censoring everything that has to do with politics. My emoji is nothing but one example of a minority who needs attention from the
media to educate people and include the non-included.

My second emoji is, as I stated before, a comment on the text "*Modifying the Universal*". One big thing I took away from reading the text was that you can never truly create diversity
that represents every single user through emojis. Emojis are extremely limited no matter how many you decide to add to the collection. You will always end up leaving somebody out of the 
equation. Emojis started off as ways to express our emotions through text messages but has now become a way to identify yourself with the "customized" emojis. whether that's skin tone or
gender expression. It forces the user to choose between these limited images and the more the companies do to include "everyone" they're just opening that the text refers to as a Pandora's
box. And Especially when all these big companies choosing the universal emoji language are placed in the United States, it's hard to believe that they know what it means to represent 
the whole globe. So the purpose of my alien emoji is to critique the way these companies are thinking. An alien looking down at humans being flustered and angry at the world. If they 
truly want to speak about diversity in culture, identity, race, politics, social and economics, that great. In fact that would be amazing, but don't do it through emojis.

