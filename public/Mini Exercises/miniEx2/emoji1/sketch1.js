// This emoji represents a lesbian latina walking a parade while holding an
// lgbt flag. The emoji is an example of one kind of minority that is rarely
// spoken of. Both in society and by big companies such as Apple, who tend to
// censor everything that has to do with any form of sexuality/ sexual content.
// Made by Clara Jassan.

function setup() {
  createCanvas(windowWidth,windowHeight);
  background(220);
}

function draw() {
  rectMode(RADIUS);
  noStroke();
  fill(94,8,107);
  rect(600,380,47,100,5,5,5,5);
  // hair

  rectMode(RADIUS);
  fill(189,141,85);
  rect(600,340,8,20);
  // the neck

  ellipseMode(RADIUS);
  fill(210,156,95);
  ellipse(600,300,40,40);
  // the head

  rectMode(RADIUS);
  fill(138,11,158);
  rect(600,270,47,25,100,100,5,5);
  fill(210,156,95);
  triangle(610,300,620,300,615,280); // chip in bangs
  // bangs

  stroke(189,141,85);
  strokeWeight(20);
  line(578,723,578,730); //left ancle
  line(622,723,622,730); //right ancle
  rectMode(CENTER);
  fill(14,18,18);
  noStroke();
  rect(578,745,27,25,5,5,0,0);
  rect(622,745,27,25,5,5,0,0);
  // shoes

  ellipseMode(RADIUS);
  fill(36,46,47);
  ellipse(600,528,40,35); //waist
  rectMode(CENTER);
  rect(578,623,35,200,20,20,5,5); // left leg
  rectMode(CENTER);
  rect(622,623,35,200,20,20,5,5); // right leg
  // the legs

  rectMode(CENTER);
  fill(42,137,144);
  rect(600, 435, 70, 150, 20, 20, 5, 5);
  // upper body

  fill(255);
  beginShape();
  vertex(470,150);
  vertex(560,30);
  vertex(665,70);
  vertex(580,180);
  endShape(); //base of the flag
  strokeWeight(25);
  stroke(255,0,0);
  line(589,174,669,78); // red
  stroke(255,137,0);
  line(569,168,649,69); // orange
  stroke(255,255,0);
  line(547,162,627,60); // yellow
  stroke(0,255,0);
  line(525,155,605,54); // green
  stroke(0,0,255);
  line(503,150,583,46); // blue
  stroke(162,0,255);
  line(484,143,561,38); // purple
  stroke(74,43,17);
  strokeWeight(5);
  line(470,150,820,250); //handle
  // pride flag

  stroke(210,156,95);
  strokeWeight(10);
  line(567,490,578,500); // hand
  stroke(42,137,144);
  strokeWeight(15);
  line(575,370,520,430); //shoulder to elbow
  stroke(42,137,144);
  strokeWeight(15);
  line(520,430,567,490); //elbow to wrist
  // left arm

  stroke(210,156,95);
  strokeWeight(10);
  line(665,220,665,205); // hand
  stroke(42,137,144);
  strokeWeight(15);
  line(628,375,660,300); //shoulder to elbow
  line(660,300,665,220); // shoulder to wrist
  // right arm





}
