![screenshot](throbber1.png) ![screenshot](throbber2.png)

[Link to miniEx3](https://clara.j.lassen.gitlab.io/ap-2020/Mini%20Exercises/miniEx3/)
I suggest waiting a bit before interacting with the program. For the best resolution I would also suggest to load the program in half the window size

[Link to repository](https://gitlab.com/clara.j.lassen/ap-2020/-/blob/master/public/Mini%20Exercises/miniEx3/RUNME.js)

**The design of my throbber - What did I want to express?**
When I started out thinking about a design for my throbber I wasn't sure where to begin. There was so many different ways to approach the task, and I had a lot of creative ideas
but wasn't sure how to create them technically. This is a limit I often meet with programming as an artist. I just want to take a pensil to the screen and draw it. But the process
of getting the computer to understand me is a completely different challenge. With my throbber I wanted to go back in time for a bit - back to when we had screensavers for windows XP
with the crazy lines crossing each other in different colors. I drew inspiration from this kind of mesmerizing art even though they're not necessarily what you'd call a throbber. 
I wanted to use this concept as a throbber and at the same time use it as a critique on how people increasingly tend to lose their patience when it comes to technology today. I thereby
plit my program in to parts - one for the throbber animation itself and the second for the interuption. The interuption by the user is being projected from the compurts perspective, as
if the computer had a mind of its own. 

**Time as a construction - How did I use it?**
The time related syntax I used for my program was primarily the speed syntax. I used this to decide how fast the circles should move per frame. Since I not only wanted to circles to move
but wanted them to draw as well, I created the background in the function setup(), allowing me to keep drawing on the same blank canvas. To make them move in this sense of time, I told
the program to move the circles' coordinates with their speed in both the x- and y-axis. I also chose for the circles to stop moving when the mouse was being pressed as a way to express
my message with the program. By this I changed the speed to 0, so it would stand still and glitch in different colors rather than move around in what I like the call the second part of the 
program. My original intention was to make the circles move again when the mouse was no longer being pressed, but even though it frustrated me that this didn't work out, I also became aware
that this is exactly how I see time behind the scenes of a computer. The intention of my design was to express how the user loses their temper when electronics don't work, which does nothing
but makes the waiting time longer and the computer more busy, having more tasks to complete. If the circles would just keep moving, time wouldn't be seen as such an abstract thing but rather
what works in the favor of the user - which in this case wasn't my intention to point out. 

**Throbbers in digital culture - What does it communicate?**
I do this a lot myself. The computer is being slow and doesn't answer my request instantly, and I become frustrated. I repeatedly click on the same button again and again in hopes that
it will complete the task faster and listen to me. But that's not how computers work. I think in today's society we have become obsessed with rushing around and everything around us needs 
to move fast. If the internet connection is being slow, we become frustrated. If the traffic is slow, we become frustrated. If the computer isn't responding, we lose our temper. This throbber
has become something we associate with involuntarily waiting for something, and this is way the sight of this phenomenon often makes us so upset. We have learned to dislike the symbol of 
waiting because we expect everything to be instant. For me the biggest issue with throbbers lie in the fact that I don't know how long I have to wait. It just keeps repeating in an almost 
taunting way. So for a change I made a program illustrating a throbber where it's not about us, but the computer. We need to stop rushing everything and take a second to wait. It's fine
if everything isn't loading this instant, and if the computer had feelings it would probably appreciate it too. Stop spamming the button when the computer isn't responding.

References used for this program:
    [Daniel Shiffman: What is an array?](https://www.youtube.com/watch?v=VIQoUghHSxU&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=27)
    [Daniel Shiffman: Function Basics](https://www.youtube.com/watch?v=wRHAitGzBrg&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=20)