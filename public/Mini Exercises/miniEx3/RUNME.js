// This is my code for the mini exercise 3. The program symbolizes a throbber
// with a mind of its own. I would suggest you to wait a bit before interacting
// with the program, and have the window be half its size.
// Then press the mouse button and watch the screen change. Made by Clara Jassan

var circles = { // declaring my variables for drawing the circles
  x: 0,
  y: 0,
  xspeed: 90,
  yspeed: -20
}

var words = ["stop", "you're not helping me", "you want me to shut down?", "that's what I thought", "WAIT NO STOP", "LEAVE ME ALONE"]; //creating an array for the words I want to use

var index = 0; // creating a variable called index, used to change the words of the array

function setup() {
  createCanvas(windowWidth, windowHeight);
    background(0);
    frameRate(20); // decreasing the framerate. I could have changed the speed instead but found a nice pace this way.
}

function draw() {
  move();
  bounce();
  display();
  // defining the functions in function draw() that I want to use
}

function move() {
  circles.x = circles.x + circles.xspeed;
  circles.y = circles.y + circles.yspeed;
  // makes the circles move with their coordinates and speed
}


function bounce() {
  if (circles.x > width || circles.x < 0) {
    circles.xspeed = circles.xspeed * -1;
  }

  if (circles.y > height || circles.y < 0) {
    circles.yspeed =  circles.yspeed * -1;
  // makes the circles turn around if they hit the end of the canvas.
  }
}


function display() {
  noStroke();
  fill(random(255), random(255), random(255));
  ellipse(circles.x, circles.y, 70,70);
  //creates the circle and randomizes the colors chosen
}


function mousePressed() {
  if (mouseIsPressed) {
    circles.xspeed = 0;
    circles.yspeed = 0; // stops the movement of the circles
    background(255,0,0); // creates a new red background
    fill(255);
    textSize(70); // text size adjustment
    text(words[index], 20, 200); // chooses the words array and placement by coordinates
    index = index + 1; // everytime the mouse is clicked, it chooses the next element in the array.
      if (index == words.length) {
        index = 0; // if the program reaches the end of the array, it resets and repeats.
    }
  }
}

//references used:
//  https://www.youtube.com/watch?v=VIQoUghHSxU&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=27
//  https://www.youtube.com/watch?v=wRHAitGzBrg&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=20
